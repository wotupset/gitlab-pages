gitlab-pages提供的網址
+ https://gitlab.com/wotupset/gitlab-pages/
+ https://wotupset.gitlab.io/gitlab-pages/

gitlab-pages提供的範本
+ https://gitlab.com/groups/pages

純HTML環境的gitlab-pages設置教學
1. 新增一個 .gitlab-ci.yml 檔案
    + 檔案內容從這裡複製 https://gitlab.com/pages/plain-html/blob/master/.gitlab-ci.yml
2. 建立一個 public 資料夾
3. 在 public 資料夾底下建立一個首頁檔案 index.html
4. 前往pipelines分頁 如果有出現綠色勾勾就是有佈署(deploy)成功
    * https://gitlab.com/wotupset/gitlab-pages/pipelines
    * ![aaaaa](/uploads/baf48da8f4d9437bb82dc30f48f800f4/aaaaa.jpg)
5. 網址是 https://帳號或組織名稱.gitlab.io/項目名稱/
    + 像是 https://wotupset.gitlab.io/gitlab-pages/






